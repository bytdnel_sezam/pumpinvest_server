import cron from 'node-cron';
import {Worker} from 'worker_threads';
import cluster from 'cluster';
import os from 'os';
import initServer from './src/server/server.js';
import startDoingCron from './src/cron/index.js';

const VARS = {
  SINGLE_THREAD: 'single-thread',
  MULTI_THREAD: 'multi-thread',
};

/**
 * @return {cpusCount} Available count of cpu cores
 */
function getCpusCount() {
  let threadType = process.argv[2];
  let cpusCount = 1;
  if (threadType == VARS.MULTI_THREAD) cpusCount = os.cpus().length-1;
  else if (threadType !== VARS.SINGLE_THREAD) {
    // throw new Error('[ERROR]: Use [st/mt] threading type')
    threadType = VARS.SINGLE_THREAD;
  }
  return cpusCount;
}

/**
 * @param {string} email
 */
function addEmailToQueue(email) {
  emailQueue.push(email);
}
/**
 * @function
 */
function setWorkerQueue() {
  if (emailQueue.length !== 0) {
    const part = emailQueue
        .splice(0, emailQueue.length >= 15 ? 15 : emailQueue.length);
    const worker = new Worker('./src/workers/mailWorker.js', {
      workerData: {
        emails: part,
      },
    });
    worker.on('exit', () => {
      console.log('worker did his work');
      worker.removeAllListeners();
    });
  }
};

const emailQueue = [];


if (cluster.isMaster) {
  console.log('Hello i am master!');
  startDoingCron();
  const cpus = getCpusCount();

  for (let cpu = 0; cpu < cpus; cpu++) {
    cluster.fork();
  }


  cluster.on('message', (worker, message) => {
    // console.log('got new message')
    // console.log(message)
    if (message.type == 'sendEmail') {
      addEmailToQueue(message.payload.email);
      console.log(emailQueue);
    }
  });

  cluster.on('exit', () => {
    console.log('oh shit');
    // cluster.fork()
  });

  cron.schedule('0,10,20,30,40,50 * * * * *', () => setWorkerQueue());
}

if (cluster.isWorker) {
  console.log('Hello i am worker!');
  const port = process.env.PORT || 4000;
  initServer(port);
}
