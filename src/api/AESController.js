import CryptoJS from 'crypto-js';
import dotenv from 'dotenv';
dotenv.config({path: '.keys.env'});

const config = {
  AES_KEY: process.env.AES_PKEY,
};

// eslint-disable-next-line valid-jsdoc
/**
 * @class with static methods
 */
class AESController {
  /**
   * @param {string} msg
   * @return {string} msg encrypted
   */
  static encrypt(msg) {
    console.log({msg});
    const encrypted = CryptoJS.AES.encrypt(msg, config.AES_KEY).toString();
    console.log({encrypted});

    return encrypted;
  }

  /**
   * @param {string} msg
   * @return {string} msg decrypted
   */
  static decrypt(msg) {
    msg = msg.split('').reduce((str, char) => {
      if (char == ' ') char = '+';
      return str += char;
    }, '');

    console.log({msg});
    const bytes = CryptoJS.AES.decrypt(msg, config.AES_KEY);
    console.log({bytes});
    const decrypted = bytes.toString(CryptoJS.enc.Utf8);
    console.log({decrypted});

    return decrypted;
  }
}

export default AESController;
