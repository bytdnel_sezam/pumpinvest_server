import nodemailer from 'nodemailer';


const transporter = nodemailer.createTransport(

    {
      host: 'smtp.yandex.ru',
      secure: false,
      port: 587,
      auth: {
        user: 'callheart@yandex.ru',
        pass: 'zmgxnredcodzdvif',
      },
    },

);

// eslint-disable-next-line valid-jsdoc
/**
 * @class with static methods
 */
class Mailer {
  /**
   * @param {object} MailOptions
   * @param {function} callback
   */
  static sendConfirmation({to, subject, text}, callback) {
    transporter.sendMail(
        {
          from: 'CallHeart <callheart@yandex.ru>',
          to,
          subject,
          text,
        },

        (err, info) => {
          if (err) callback(err);
          else callback(null, info);
        },
    );
  }
}

export default Mailer;
