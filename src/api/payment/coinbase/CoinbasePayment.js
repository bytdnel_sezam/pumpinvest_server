import coinbase from 'coinbase-commerce-node';
import dotenv from 'dotenv';

dotenv.config({path: '.keys.env'});

const config = {
  COINBASE_API_KEY: process.env.COINBASE_API_KEY,
};

const {Client} = coinbase;
const {Charge} = coinbase.resources;

Client.init(config.COINBASE_API_KEY);

/**
 * Wrapping Coinbase Api
 * @class CoinbasePayment
 * @return {Promise<object>}
 */
class CoinbasePayment {
  /**
   * @param {string | number} amount
   * @return {Promise<object>}
   */
  static async createCharge(amount) {
    try {
      const chargeOptions = {
        name: 'Пополнение кошелька в Pumpinvest',
        description: 'Самый крутой способ пополнить баланс!',
        local_price: {
          amount,
          currency: 'USD',
        },
        pricing_type: 'fixed_price',
      };
      const ChargePaymentObject = await Charge.create(chargeOptions);

      return ChargePaymentObject;
    } catch (chargeError) {
      throw new Error(chargeError);
    }
  }
}

export default CoinbasePayment;
