import QiwiBillPaymentsAPI from '@qiwi/bill-payments-node-js-sdk';
import {asyncApi} from 'node-qiwi-api';
import dotenv from 'dotenv';
dotenv.config({path: '.keys.env'});

const AdminAccountQiwiApi = asyncApi;

const QIWI_KEYS = {
  PUBLIC: process.env.QIWI_PUBLIC,
  PRIVATE: process.env.QIWI_PRIVATE,
  ACCOUNT: process.env.QIWI_ACCOUNT_KEY,
};

// eslint-disable-next-line new-cap
const qiwiAdminApi = new AdminAccountQiwiApi(QIWI_KEYS.ACCOUNT);
const qiwiPaymentApi = new QiwiBillPaymentsAPI(QIWI_KEYS.PRIVATE);

qiwiAdminApi.getWallet = () => '79913009263';
qiwiPaymentApi.getPublicKey = () => QIWI_KEYS.PUBLIC;

export {
  qiwiPaymentApi,
  qiwiAdminApi,
};
