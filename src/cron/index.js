import currencyConverterJob from './jobs/currencyConverter.js';
import systemRevenueJob from './jobs/systemRevenueOnUid.js';
import referralFeeJob from './jobs/referralFee.js';

// const currencyConverterJob = require('./jobs/currencyConverter.js')
// const systemRevenueJob = require('./jobs/systemRevenueOnUid.js')
// const referralFeeJob = require('./jobs/referralFee.js')

const jobs = [
  currencyConverterJob,
  systemRevenueJob,
  referralFeeJob,
];

/**
 * @function
 */
export default function startDoingCron() {
  jobs.forEach((job) => job());
}
