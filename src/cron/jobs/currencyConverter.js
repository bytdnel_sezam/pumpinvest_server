import currencyConverter from 'ecb-exchange-rates';
import c2p from 'callback-promise';
import Promise from 'es6-promise';
import cron from 'node-cron';
import db from '../../database/database.js';

c2p.Promise = Promise;

currencyConverter.convert = c2p(currencyConverter.convert);

/**
 * @constructor
 * @method convert
 */
class CurrencyConverter {
  /**
   *
   * @param {string} fromCurrency ISO currency string
   * @param {string} toCurrency ISO currency string
   */
  constructor(fromCurrency, toCurrency) {
    this.fromCurrency = fromCurrency;
    this.toCurrency = toCurrency;
    this.accuracy = 2;
  }
  /**
   *
   * @param {number} amount wrapping function
   * @return {number} convertedAmount
   */
  async convert(amount) {
    const convertedAmount = await currencyConverter.convert({
      ...this,
      amount,
    });

    return convertedAmount;
  }
}

/**
 * @function currencyConverterJob
 */
export default function currencyConverterJob() {
  console.log('currencyConverterJob initialized.');
  cron.schedule('0 0 0,8,16 * * *', async () => {
    try {
      const USD_RUB = await new CurrencyConverter('USD', 'RUB').convert(1);
      console.log({USD_RUB});

      await db.query(
          `UPDATE currency_ratios SET val = $1, date_update = $2 
          WHERE ratio = $3`,
          [
            USD_RUB.exchangeRate.toFixed(2),
            (new Date()).toISOString(),
            'usd_rub',
          ],
      );
    } catch (e) {
      console.log('cronjob currency ratio converter error'. e);
    }
  });
}

