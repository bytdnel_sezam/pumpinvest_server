import db from '../../database/database.js';
import cron from 'node-cron';
import {types} from '../../helpers/index.js';
const {
  TRANSACTION_TYPE,
  TRANSACTION_STATUS,
} = types;

/**
 * This func returns a number type of percent depending on level
 * @function getTransactionPercent
 * @param {string} level
 * @return {number} Example - 0.03
 */
function getTransactionPercent(level) {
  if (level == '1') {
    return 0.1;
  } else if (level == '2') {
    return 0.03;
  } else if (level == '3') {
    return 0.01;
  }
}

/**
 * @param {object} user
 */
async function addTransaction(user) {
  console.log('-----------');
  console.log({user});
  const result = await db.query(
      'SELECT balance FROM wallets WHERE uid = $1',
      [user.uid],
  );
  user.balance = result.rows[0].balance;
  console.log({user}, 'WITH BALANCE');

  const feePercent = getTransactionPercent(user.level);
  const transactionAmount = Number((user.childBalance * feePercent).toFixed(2));
  const balanceAfterFee = Number((user.balance + transactionAmount).toFixed(2));
  console.log({feePercent, transactionAmount, balanceAfterFee});
  const dateCreated = (new Date()).toISOString();
  // eslint-disable-next-line max-len
  const additionallyData = `{"info": "transaction for uid:${user.uid}, level:${user.level}"}`;

  db.query(
      `INSERT INTO transactions 
        (uid, transaction_amount,transaction_type_id, 
        transaction_status_id, date_created, currency_type, additionally_data)
      VALUES ($1, $2, $3, $4, $5, $6, $7)`,
      [
        user.uid,
        transactionAmount,
        TRANSACTION_TYPE.ID_BY_VALUE.REFERRAL_FEE,
        TRANSACTION_STATUS.ID_BY_VALUE.SUCCESS,
        dateCreated,
        'USD',
        additionallyData,
      ],

      async (transactionErr) => {
        if (transactionErr) {
          console.log({transactionErr});
        } else {
          await db.query(
              'UPDATE wallets SET balance = $1 WHERE uid = $2',
              [balanceAfterFee, user.uid],
          );
        }
      },
  );
}

/**
 * @function
 */
export default function referralFeeJob() {
  console.log('referralFeeJob initialized.');
  cron.schedule('0 0 1 * * *', async () => {
    console.log('START TRANSACTIONS!');

    const usersRefDependency = (await db.query(
        `SELECT uid, balance, father_id_l1, father_id_l2, father_id_l3 
        FROM 
        (SELECT 
          u.uid, u.role_id, 
          balance, rf.uid as rf_uid, 
          rf.father_id_l1, rf.father_id_l2, rf.father_id_l3
        FROM users u LEFT JOIN wallets w ON u.uid = w.uid
        LEFT JOIN referral_fathers rf ON u.uid = rf.uid) 
        AS ref_dependency
        WHERE (rf_uid) IS NOT NULL 
        AND role_id = 1 OR role_id = 3 AND balance > 0`,
    )).rows;

    console.log({usersRefDependency});

    if (usersRefDependency.length > 0) {
      for (const user of usersRefDependency) {
        let fathers = [
          {uid: user.father_id_l1, level: '1', childBalance: user.balance},
          {uid: user.father_id_l2, level: '2', childBalance: user.balance},
          {uid: user.father_id_l3, level: '3', childBalance: user.balance},
        ];

        console.log({fathers});

        fathers = fathers.filter((v) => v.uid);

        fathers.forEach(async (father) => {
          console.log('ADD TRANSACTION FOR', father, 'FROM', user.uid);
          await addTransaction(father);
        });
      }
    }
  });
};
