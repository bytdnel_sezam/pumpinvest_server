import db from '../../database/database.js';
import cron from 'node-cron';
import {types} from '../../helpers/index.js';

const {
  TRANSACTION_TYPE,
  TRANSACTION_STATUS,
} = types;

/**
 * @param {number} balance
 * @return {number} number type percent
 */
function getInvestmentPercent(balance) {
  if (balance <= 10) {
    return 0.01;
  } else if (balance <= 50) {
    return 0.02;
  } else {
    return 0.03;
  }
}

/**
 * @function systemRevenueJob
 */
export default function systemRevenueJob() {
  console.log('systemRevenueJob initialized.');
  cron.schedule('0 0 0 * * *', async () => {
    console.log('SYSTEM_REVENUE_ON_UID');
    const uidResult = await db.query(
        `SELECT u.uid, balance, u.role_id 
      FROM users u LEFT JOIN wallets w ON u.uid = w.uid`,
    );
    const usersInfo = uidResult.rows;

    for (const user of usersInfo) {
      if (
        (user.role_id == 3 || user.role_id == 1) &&
                user.balance > 0
      ) {
        const investmentPercent = getInvestmentPercent(user.balance);
        const transactionAmount = Number(
            (user.balance * investmentPercent).toFixed(2),
        );
        const balanceAfterRevenue = Number(
            (user.balance + transactionAmount).toFixed(2),
        );
        const dateCreated = (new Date()).toISOString();

        db.query(
            `INSERT INTO transactions 
              (uid, 
              transaction_amount, 
              transaction_type_id, 
              transaction_status_id, 
              date_created, 
              currency_type)
            VALUES ($1, $2, $3, $4, $5, $6)`,
            [
              user.uid,
              transactionAmount,
              TRANSACTION_TYPE.ID_BY_VALUE.SYSTEM_REVENUE,
              TRANSACTION_STATUS.ID_BY_VALUE.SUCCESS,
              dateCreated,
              'USD',
            ],
            (transactionErr) => {
              if (transactionErr) {
                console.log({transactionErr});
              } else {
                db.query(
                    'UPDATE wallets SET balance = $1 WHERE uid = $2',
                    [balanceAfterRevenue, user.uid],
                );
              }
            },
        );
      }
    }
  });
};
