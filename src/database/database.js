import pg from 'pg';
import dotenv from 'dotenv';

dotenv.config({path: '.postgre.env'});

const config = {
  user: process.env.PG_USER,
  password: Number(process.env.PG_PASSWORD),
  host: process.env.PG_HOST,
  port: Number(process.env.PG_PORT),
  database: process.env.PG_DATABASE,
};

const pool = new pg.Pool({
  user: config.user,
  password: config.password,
  host: config.host,
  port: config.port,
  database: config.database,
});

export default pool;
