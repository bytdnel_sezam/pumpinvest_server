import responseWrapper from './responseWrapper.js';

/**
 * @param {object} req
 * @param {object} res
 * @param {function} next
 */
export default function allowOnlyAdmin(req, res, next) {
  const userInfo = req.JWT.payload;
  if (userInfo.role_id !== 1) {
    responseWrapper(res, 403, {error: new Error('Forbidden')});
  } else next();
}
