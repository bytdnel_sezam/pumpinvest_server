/**
 *
 * @param {object} ExpressResponse
 * @param {number} status
 * @param {object} content
 */
export default function responseWrapper(
    ExpressResponse,
    status,
    content,
) {
  const contentError = content.error;
  const response = {
    status,
    payload: {},
  };

  if (contentError) {
    response.error = true;
    response.payload.error = {
      message: contentError.message,
      name: contentError.name,
      fileName: contentError.fileName,
      lineNumber: contentError.lineNumber,
      stack: contentError.stack,
    };
  } else {
    response.error = false;
    response.payload = {...content};
  }
  ExpressResponse.status(status).send(response);
}
