export const ROLE_TYPE = {
  VALUE_BY_ID: {
    1: 'admin',
    2: 'unconfirmed_user',
    3: 'confirmed_user',
  },
  ID_BY_VALUE: {
    ADMIN: 1,
    UNCONFIRMED: 2,
    CONFIRMED: 3,
  },
};

export const TRANSACTION_TYPE = {
  VALUE_BY_ID: {
    1: 'referral_fee',
    2: 'deposit_income',
    3: 'system_revenue',
    4: 'withdrawal_request',
    5: 'withdrawal_confirmed',
    6: 'withdrawal_canceled',
  },
  ID_BY_VALUE: {
    REFERRAL_FEE: 1,
    DEPOSIT_INCOME: 2,
    SYSTEM_REVENUE: 3,
    WITHDRAWAL_REQUEST: 4,
    WITHDRAWAL_CONFIRMED: 5,
    WITHDRAWAL_CANCELED: 6,
  },
};

export const TRANSACTION_STATUS = {
  VALUE_BY_ID: {
    1: 'pending',
    2: 'success',
    3: 'fail',
  },
  ID_BY_VALUE: {
    PENDING: 1,
    SUCCESS: 2,
    FAIL: 3,
  },
};
