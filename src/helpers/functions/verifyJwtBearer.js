
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import {wrappedResponse} from '../../helpers/index.js';

dotenv.config({path: '.keys.env'});

const config = {
  JWT_SECRET: process.env.JWT_SECRET,
};

/**
 * @param {object} req
 * @param {object} res
 * @param {function} next
 */
export default function verifyJwtBearer(req, res, next) {
  const bearerHeader = req.headers['authorization'];

  if (bearerHeader) {
    const [bearer, token] = bearerHeader.split(' ');
    console.log({bearer, token});

    if (bearer === 'Bearer') {
      jwt.verify(token, config.JWT_SECRET, (err, authData) => {
        req.JWT = {};

        if (err) {
          console.log(err);
          wrappedResponse(res, 403, {error: new Error('Forbidden')});
        } else {
          req.JWT = {
            statusCode: 200,
            message: 'JWT verified',
            payload: authData,
          };
          next();
        }
      });
    } else wrappedResponse(res, 400, {error: new Error('Bad request')});
  } else wrappedResponse(res, 400, {error: new Error('Bad request')});
};

