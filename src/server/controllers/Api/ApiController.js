import {wrappedResponse} from '../../../helpers/index.js';
import {
  canWithdraw,
  getBalance,
  createWithdrawalRequest,
  getTransactions,
  getTransaction,
  getAllWithdrawalRequestTransactions,
  withdrawMoney,
  updateRequestTransactionToConfirmed,
  // updateRequestTransactionToCanceled,
  reduceUserBalance,
} from './ApiControllerFunctions.js';

/**
 * @class ApiController
 * @route /api
 */
export default class ApiController {
  /**
   * @method GET
   * @route /api/getUserTransactions
   *
   * @param {object} req
   * @param {object} res
   */
  static async getUserTransactions(req, res) {
    const userInfo = req.JWT.payload;
    try {
      const transactions = await getTransactions(userInfo.uid);
      wrappedResponse(res, 200, {transactions});
    } catch (error) {
      wrappedResponse(res, 500, {error});
    }
  }

  /**
   * @method GET
   * @route /api/getAllWithdrawalRequestTransactions
   *
   * @param {object} req
   * @param {object} res
   */
  static async getAllRequestedTransactions(req, res) {
    try {
      const transactions = await getAllWithdrawalRequestTransactions();
      wrappedResponse(res, 200, {transactions});
    } catch (error) {
      wrappedResponse(res, 500, {error});
    }
  }

  /**
   * @method POST
   * @route /api/withdrawalRequest
   *
   * @param {object} req
   * @param {object} res
   */
  static async withdrawalRequest(req, res) {
    const userInfo = req.JWT.payload;
    const {amount, type, address} = req.body;
    const withdrawalInfo = {type, address};
    console.log({userInfo, body: req.body});
    try {
      const balance = await getBalance(userInfo.uid);
      if (canWithdraw(amount, balance)) {
        await createWithdrawalRequest(
            userInfo.uid, amount, withdrawalInfo,
        );
        wrappedResponse(res, 200, {message: 'OK'});
      }
    } catch (error) {
      wrappedResponse(res, 500, {error});
    }
  }

  /**
   * @method POST
   * @route /api/withdrawMoney
   *
   * @param {object} req
   * @param {object} res
   */
  static async withdrawMoney(req, res) {
    try {
      const userInfo = req.JWT.payload;
      const {transactionId} = req.body;
      const transactionInfo = await getTransaction(transactionId);
      console.log({transactionInfo});
      // await withdrawMoney(
      //     transactionInfo.transaction_amount,
      //     transactionInfo.additionally_data.type,
      //     transactionInfo.additionally_data.address,
      // );
      await updateRequestTransactionToConfirmed(
          transactionId,
      );
      await reduceUserBalance(
          userInfo.uid,
          transactionInfo.transaction_amount,
      );
      wrappedResponse(res, 200, {message: 'OK'});
    } catch (error) {
      wrappedResponse(res, 500, {error});
    }
  }
}
