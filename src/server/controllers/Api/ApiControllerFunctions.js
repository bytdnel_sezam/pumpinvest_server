import db from '../../../database/database.js';
import {types} from '../../../helpers/index.js';
import {qiwiAdminApi} from '../../../api/payment/qiwi/qiwi.js';

const {
  TRANSACTION_TYPE,
  TRANSACTION_STATUS,
} = types;

/**
 * Wrapper function. If can withdraw returns balance
 * @throw DB Errors
 * @param {number} amount
 * @param {number} balance
 * @return {balance}
 */
export function canWithdraw(amount, balance) {
  if (amount <= balance) {
    return balance;
  } else {
    throw new Error('Not enough balance to withdraw!');
  }
}

/**
 * @async works with DB
 * @param {number} uid
 * @return {Promise<number>} User balance
 */
export async function getBalance(uid) {
  try {
    const balanceQuery = await db.query(
        'SELECT balance FROM wallets WHERE uid = $1',
        [uid],
    );

    return balanceQuery.rows[0].balance;
  } catch (balanceErr) {
    throw new Error(balanceErr);
  }
}

/**
 * Inserts new [withdrawalRequest] transaction into DB
 * @param {number} uid
 * @param {number} amount
 * @param {object} withdrawalInfo
 */
export async function createWithdrawalRequest(uid, amount, withdrawalInfo) {
  try {
    const defaultStatus = TRANSACTION_STATUS.ID_BY_VALUE.SUCCESS;
    const withdrawalType = TRANSACTION_TYPE.ID_BY_VALUE.WITHDRAWAL_REQUEST;
    const defaultCurrency = 'USD';
    const stringWithdrawalInfo = JSON.stringify(withdrawalInfo);
    await db.query(
        `INSERT INTO transactions 
        (uid, 
        transaction_amount, 
        transaction_type_id, 
        transaction_status_id,
        currency_type,
        additionally_data)
      VALUES ($1, $2, $3, $4, $5, $6)`,
        [
          uid,
          amount,
          withdrawalType,
          defaultStatus,
          defaultCurrency,
          stringWithdrawalInfo,
        ],
    );
  } catch (transactionErr) {
    throw new Error(transactionErr);
  }
}

/**
 *
 * @param {number} uid
 * @return {Promise<array>}
 */
export async function getTransactions(uid) {
  try {
    const queryResult = await db.query(
        'SELECT * FROM transactions WHERE uid = $1',
        [uid],
    );
    const transactions = queryResult.rows;
    return transactions;
  } catch (queryTransactionsErr) {
    throw new Error(queryTransactionsErr);
  }
};

/**
 * @param {number} transactionId
 * @return {object} transaction
 */
export async function getTransaction(transactionId) {
  try {
    const queryResult = await db.query(
        `SELECT * FROM transactions WHERE transaction_id = $1`,
        [transactionId],
    );
    const transaction = queryResult.rows[0];
    return transaction;
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 *
 */
export async function getAllWithdrawalRequestTransactions() {
  try {
    const queryResult = await db.query(
        `
        SELECT * FROM
        (
          SELECT 
            T.transaction_id AS id,
            T.uid AS uid,
            U.email AS email,
            W.balance AS balance,
            T.transaction_amount AS amount,
            T.date_created AS date_created,
            T.additionally_data AS additionally_data,
            T.transaction_type_id AS type_id
          FROM transactions T
          LEFT JOIN uids U ON T.uid = U.uid
          LEFT JOIN wallets W ON U.uid = W.uid
        ) AS WithdrawalRequestTransactions
        WHERE type_id = $1`,
        [types.TRANSACTION_TYPE.ID_BY_VALUE.WITHDRAWAL_REQUEST],
    );
    const transactions = queryResult.rows;
    return transactions;
  } catch (error) {
    wrappedResponse(res, 500, {error});
  }
}

/**
 *
 * @param {number} amount
 * @param {string} paymentType
 * @param {string} paymentAddress
 */
export async function withdrawMoney(
    amount,
    paymentType,
    paymentAddress,
) {
  try {
    switch (paymentType) {
      case 'card': {
        await qiwiAdminApi.toCard({
          amount,
          comment: 'Вывод средств Pumpinvest',
          account: paymentAddress,
        });
      }
        break;
      case 'qw': {
        await qiwiAdminApi.toWallet({
          amount,
          comment: 'Вывод средств Pumpinvest',
          account: paymentAddress,
        });
      }
        break;
      default: {
        throw new Error('There is no such paymentType');
      }
    }
  } catch (paymentErr) {
    throw new Error(
        paymentErr.message,
    );
  }
}

/**
 *
 * @param {number} transactionId
 */
export async function updateRequestTransactionToConfirmed(transactionId) {
  try {
    await db.query(
        `UPDATE transactions
        SET transaction_type_id = $1
        WHERE transaction_id = $2`,
        [
          types.TRANSACTION_TYPE.ID_BY_VALUE.WITHDRAWAL_CONFIRMED,
          transactionId,
        ],
    );
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 *
 * @param {number} uid
 * @param {number} amount
 */
export async function reduceUserBalance(uid, amount) {
  try {
    await db.query(
        `UPDATE wallets
        SET balance = ROUND((balance - $1)::numeric, 6)
        WHERE uid = $2`,
        [amount, uid],
    );
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

