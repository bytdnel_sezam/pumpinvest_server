import db from '../../../database/database.js';
import AESController from '../../../api/AESController.js';
import {wrappedResponse} from '../../../helpers/index.js';
import dotenv from 'dotenv';

import {
  getValidUserSchema,
  getUserInfoByEmail,
  validatePassword,
  getToken,
  getBalance,
  getReferralsCount,
  createUserWallet,
  setReferralDependency,
  createUser,
  getUid,
  genHashPassword,
  // eslint-disable-next-line no-unused-vars
  sendConformationalMail,
  truncateUser,
} from './Auth0ControllerFunctions.js';

dotenv.config({path: '.keys.env'});

// eslint-disable-next-line valid-jsdoc
/**
 * Express Route /api methods
 */
class Auth0Controller {
  /**
     * POST Route /register
     * @param {object} req
     * @param {object} res
     */
  static async register(req, res) {
    let incomingUserData;
    const undoQueries = [];
    try {
      incomingUserData = getValidUserSchema(req.body);
      const {uid, undoUid} = await getUid(incomingUserData);
      incomingUserData.uid = uid;
      const undoWallet = await createUserWallet(uid);
      undoQueries.unshift(undoWallet);

      if (incomingUserData.father_id) {
        const undoReferralDependency = await setReferralDependency(
            incomingUserData.uid,
            incomingUserData.father_id,
        );
        undoQueries.unshift(undoReferralDependency);
      }
      const hashedPassword = await genHashPassword(
          incomingUserData.password,
      );

      incomingUserData.password = hashedPassword;
      incomingUserData.balance = 0;

      const {createdUser, undoUser} = await createUser(incomingUserData);
      undoQueries.push(undoUser, undoUid);
      const userInfo = {...incomingUserData, ...createdUser};

      const token = await getToken(userInfo);
      // sendConformationalMail(userInfo.email);

      wrappedResponse(res, 200, {
        message: 'Authorized',
        token,
        userInfo,
      });
    } catch (userValidationErr) {
      await truncateUser(undoQueries);
      wrappedResponse(res, 500, {
        error: userValidationErr,
      });
    };
  };

  /**
     *
     * @param {object} req
     * @param {object} res
     */
  static async fetchAuth(req, res) {
    const userInfo = req.JWT.payload;
    // userInfo.password = undefined;
    try {
      const balance = await getBalance(userInfo.uid);
      const referralsCount = await getReferralsCount(userInfo.uid);

      userInfo.balance = balance;
      userInfo.referrals_l1_count = referralsCount.referrals_l1_count;
      userInfo.referrals_l2_count = referralsCount.referrals_l2_count;
      userInfo.referrals_l3_count = referralsCount.referrals_l3_count;

      wrappedResponse(res, 200, {userInfo});
    } catch (error) {
      wrappedResponse(res, 500, {error});
    }
  }

  /**
     * @param {object} req
     * @param {object} res
     */
  static async login(req, res) {
    const credentials = req.body;
    console.log({credentials});
    try {
      const userInfo = await getUserInfoByEmail(credentials.email);
      const hashedPassword = userInfo.password;
      const isValid = await validatePassword(
          credentials.password, hashedPassword,
      );
      if (isValid) {
        const token = await getToken(userInfo);
        const balance = await getBalance(userInfo.uid);
        userInfo.balance = balance;
        wrappedResponse(res, 200, {
          message: 'Authorized',
          token,
          userInfo,
        });
      } else {
        wrappedResponse(res, 401, {error: new Error('Unauthorized')});
      }
    } catch (error) {
      wrappedResponse(res, 500, {error});
    }
  }

  /**
     * @param {object} req
     * @param {object} res
     */
  static confirmUser(req, res) {
    const params = req.query;

    if (!params.token) {
      wrappedResponse(
          res,
          500,
          {error: new Error('There is no [token]')},
      );
    } else {
      let email;
      try {
        email = AESController.decrypt(params.token);
        db.query(
            `UPDATE users SET role_id = '${3}'
              WHERE email = '${email}' AND role_id = '${2}'
              RETURNING email, nickname, (
                  SELECT role_type FROM roles WHERE role_id = '${3}'
              ) AS role_type`,
            (updateUserErr, queryResult) => {
              if (updateUserErr) {
                wrappedResponse(res, 500, {error: updateUserErr});
              } else {
                if (queryResult.rows.length == 0) {
                  wrappedResponse(
                      res,
                      500,
                      {error: new Error('Already confirmed')},
                  );
                } else {
                  wrappedResponse(
                      res,
                      200,
                      {message: 'User confirmed', ...queryResult.rows[0]},
                  );
                }
              }
            },

        );
      } catch (e) {
        wrappedResponse(res, 500, {error: err});
      }
    }
  }
}

export default Auth0Controller;
