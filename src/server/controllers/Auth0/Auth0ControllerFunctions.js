import db from '../../../database/database.js';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import {getBalance} from '../Api/ApiControllerFunctions.js';

dotenv.config({path: '.keys.env'});

const config = {
  JWT_SECRET: process.env.JWT_SECRET,
};

/**
 * @async
 * @param {string} email
 */
export async function getUserInfoByEmail(email) {
  try {
    const queryResult = await db.query(
        `SELECT * FROM users WHERE email = $1`,
        [email],
    );
    const exists = queryResult.rows.length > 0 ? true : false;
    if (exists) {
      const userInfo = queryResult.rows[0];
      return userInfo;
    } else {
      throw new Error('There is no user with gived credentials');
    }
  } catch (error) {
    throw new Error(error);
  }
}

/**
 *
 * @param {string} password
 * @param {string} hashedPassword
 * @return {Promise<boolean>} isValid
 */
export async function validatePassword(password, hashedPassword) {
  try {
    const isValid = await bcrypt.compare(password, hashedPassword);
    return isValid;
  } catch (comparePasswordsErr) {
    throw new Error(comparePasswordsErr);
  }
}

/**
 *
 * @param {object} user
 * @return {Promise<string>} Token
 */
export function getToken(user) {
  return new Promise((resolve, reject) => {
    try {
      user.password = null;
      const expiresIn = Date.now() + 6*60*60*1000;
      jwt.sign(
          user,
          config.JWT_SECRET,
          {expiresIn},
          function signUserInfo(signErr, token) {
            if (signErr) {
              reject(signErr);
            } else {
              resolve(token);
            }
          });
    } catch (err) {
      reject(err);
    }
  });
}

/**
 * IncomingUserData Validator
 * @param {object} data
 * @return {object} User
 */
export function getValidUserSchema(data) {
  const userSchema = {};

  if (data.email && typeof data.email == 'string') {
    userSchema.email = data.email;
  } else throw new Error('Invalid [email]');

  if (data.password && typeof data.password == 'string') {
    userSchema.password = data.password;
  } else throw new Error('Invalid [password]');

  if (data.nickname) {
    if (typeof data.nickname !== 'string') {
      throw new Error('Invalid [nickname]');
    }
    userSchema.nickname = data.nickname;
  } else {
    userSchema.nickname = null;
  }

  if (data.father_id) {
    if (typeof data.father_id !== 'number') {
      throw new Error('Invalid [fatherId]');
    }
    userSchema.father_id = data.father_id;
  }

  return userSchema;
}

/**
 *
 * @param {number} uid
 * @return {Promise<object>}
 */
export async function getReferralsCount(uid) {
  try {
    const queryResult = await db.query(
        `SELECT
        referrals_l1_count, referrals_l2_count, referrals_l3_count
        FROM users WHERE uid = $1`,
        [uid],
    );

    const referralsCount = {
      referrals_l1_count: queryResult.rows[0].referrals_l1_count,
      referrals_l2_count: queryResult.rows[0].referrals_l2_count,
      referrals_l3_count: queryResult.rows[0].referrals_l3_count,
    };

    return referralsCount;
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 * @async Function which get/set wallet for user
 * @param {number} uid
 * @return {Promise<array>} Array of undo queries
 */
export async function createUserWallet(uid) {
  try {
    // for (let currencyType of ['USD']) {
    await db.query(
        `INSERT INTO wallets (uid, currency_type, balance) 
            VALUES ($1, $2, $3)`,
        [uid, 'USD', 0],
    );
    return [`DELETE FROM wallets WHERE uid = ${uid}`];
    // }
  } catch (walletInsertErr) {
    throw new Error(walletInsertErr);
  }
}

/**
 *
 * @param {string} uid
 * @return {Promise<void>}
 */
export async function getReferralFather(uid) {
  console.log({uid});
  try {
    if (uid == null) return null;
    const queryResult = await db.query(
        `SELECT father_id FROM referrals WHERE child_id = $1`,
        [uid],
    );
    console.log({queryResult: queryResult.rows});
    const exists = queryResult.rows.length > 0 ? true : false;

    if (exists) {
      const fatherId = queryResult.rows[0].father_id;
      return fatherId;
    } else return null;
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 * @async
 * @param {number} uid
 * @param {string | number} level
 */
export async function incrementReferralsCount(uid, level) {
  try {
    if (!uid) return;
    await db.query(
        `UPDATE users 
        SET referrals_l${level}_count = referrals_l${level}_count + 1 
        WHERE uid = $1`,
        [uid],
    );
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 * @async
 * Add to db info about referral relations
 * @param {number} childId
 * @param {number} fatherId
 * @return {Promise<array>} Array of undo queries
 */
export async function setReferralDependency(childId, fatherId) {
  console.log({childId, fatherId});
  try {
    await db.query(
        'INSERT INTO referrals (father_id, child_id) VALUES ($1, $2)',
        [fatherId, childId],
    );

    const fatherIdL1 = fatherId;
    console.log({fatherIdL1});
    const fatherIdL2 = await getReferralFather(fatherIdL1);
    console.log({fatherIdL2});
    const fatherIdL3 = await getReferralFather(fatherIdL2);
    console.log({fatherIdL3});

    await db.query(
        `INSERT INTO referral_fathers
          (uid, father_id_l1, father_id_l2, father_id_l3)
        VALUES ($1, $2, $3, $4)`,
        [childId, fatherIdL1, fatherIdL2, fatherIdL3],
    );

    await incrementReferralsCount(fatherIdL1, 1);
    await incrementReferralsCount(fatherIdL2, 2);
    await incrementReferralsCount(fatherIdL3, 3);

    const deleteQueries = [
      `DELETE FROM referrals 
      WHERE father_id = ${fatherId} AND child_id = ${childId}`,
      `DELETE FROM referral_fathers WHERE child_id = ${childId}`,
    ];

    return deleteQueries;
  } catch (refInsertErr) {
    throw new Error(refInsertErr);
  }
}

/**
 *
 * @param {*} user
 * @return {Promise<object>}
 */
export async function createUser(user) {
  try {
    const queryResult = await db.query(
        `INSERT INTO users 
          (uid, email, password, nickname, auth_id)
        VALUES ($1, $2, $3, $4, $5)
        RETURNING *`,
        [user.uid, user.email, user.password, user.nickname, 2],
    );
    const createdUser = queryResult.rows[0];
    return {
      createdUser,
      undoUser: [
        `DELETE FROM users WHERE uid = ${user.uid}`,
      ],
    };
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 *
 * @param {object} user
 * @return {Promise<object>}
 */
export async function getUid(user) {
  try {
    const queryResult = await db.query(
        'INSERT INTO uids (email) VALUES ($1) RETURNING uid',
        [user.email],
    );

    const uid = queryResult.rows[0].uid;

    return {
      uid,
      undoUid: [`DELETE FROM uids WHERE uid = ${uid}`],
    };
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 *
 * @param {string} password
 * @return {Promise<string>}
 */
export async function genHashPassword(password) {
  return new Promise((resolve, reject) => {
    try {
      const rounds = 8;
      bcrypt.genSalt(rounds, (saltErr, salt) => {
        if (saltErr) reject(saltErr);
        else {
          bcrypt.hash(password, salt, (hashErr, hash) => {
            if (hashErr) reject(hashErr);
            else resolve(hash);
          });
        }
      });
    } catch (genErr) {
      throw new Error(genErr);
    }
  });
}


/**
 * @function
 * @param {string} email
 */
export function sendConformationalMail(email) {
  process.send({
    type: 'sendEmail', payload: {email},
  });
}

/**
 *
 * @param {array} undoQueries
 */
export async function truncateUser(undoQueries) {
  for (const part of undoQueries) {
    for (const query of part) {
      await db.query(query);
    }
  }
}

export {getBalance};
