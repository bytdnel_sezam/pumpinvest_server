import Cookies from 'cookies';
import db from '../../../database/database.js';

/**
 * @class Auth2Controller
 */
class Auth2Controller {
  /**
   * @method GET
   * @param {object} req
   * @param {object} res
   */
  static googleCallback(req, res) {
    console.log('GET_GOOGLE_CALLBACK');
    const cookies = new Cookies(req, res);
    const refid = cookies.get('refid') || null;
    const googleUserInfo = req.user;
    const email = googleUserInfo.emails[0].value;
    console.log(googleUserInfo.emails);
    const TRANSACTION_TYPES = {
      REFERRAL_FATHERS: 'referral_fathers',
      WALLET: 'wallet',
    };
    const UserCreationTransaction = [
      () => {
        if (refid) {
          db.query(
              `SELECT 
                user_id, 
                referral_father_1, 
                referral_father_2, 
                referral_father_3 
              FROM users WHERE user_id = $1`, [refid], (err, result) => {
                if (err) next(err); // handling error ??
                else {
                  const fatherFathers = result.rows[0];
                  console.log({fatherFathers});
                  const userFathers = [
                    fatherFathers.user_id,
                    fatherFathers.referral_father_1,
                    fatherFathers.referral_father_2,
                  ];
                  nextTransactionState({
                    type: TRANSACTION_TYPES.REFERRAL_FATHERS,
                    content: {
                      referral_father_1: userFathers[0],
                      referral_father_2: userFathers[1],
                      referral_father_3: userFathers[2],
                    },
                    undoQuery: false,
                  });
                }
              });
        } else {
          nextTransactionState({
            type: TRANSACTION_TYPES.REFERRAL_FATHERS,
            content: {
              referral_father_1: null,
              referral_father_2: null,
              referral_father_3: null,
            },
            undoQuery: false,
          });
        }
      },
      () => {
        db.query(
            // eslint-disable-next-line quotes
            "INSERT INTO wallets (balance) VALUES (\'0\') RETURNING wallet_id",
            (err, result) => {
              if (err) next(err);
              else {
                const walletId = result.rows[0].wallet_id;
                nextTransactionState({
                  type: TRANSACTION_TYPES.WALLET,
                  content: walletId,
                  undoQuery: `DELETE FROM wallets 
                  WHERE wallet_id = '${walletId}'`,
                });
              }
            });
      },
    ];

    const succeededTransactionSteps = [];

    /**
     * Something like redux dispatcher
     * @param {object} obj
     */
    function nextTransactionState(obj) {
      succeededTransactionSteps.push(obj);
      const successTransactionLen = succeededTransactionSteps.length;
      const userTransactionLen = UserCreationTransaction.length;
      if (successTransactionLen == userTransactionLen) {
        createUser(succeededTransactionSteps);
      }
    }
    /**
     * @param {array} steps
     */
    function createUser(steps) {
      console.log({steps});
      const wallet = steps
          .find((step) => step.type == TRANSACTION_TYPES.WALLET);
      const fathers = steps
          .find((step) => step.type == TRANSACTION_TYPES.REFERRAL_FATHERS);
      console.log({wallet, fathers});
      db.query(
          `INSERT INTO users
          (email,
          wallet_id,
          role_id,
          referral_father_1,
          referral_father_2,
          referral_father_3,
          auth_id)
          VALUES ($1, $2, $3, $4, $5, $6, $7)
          RETURNING *`,
          [
            email, // 1
            wallet.content.wallet_id, // 4
            3, // 5
            fathers.content.referral_father_1, // 6
            fathers.content.referral_father_2, // 7
            fathers.content.referral_father_3, // 8
            1, // 9
          ],
          (err, queryResult) => {
            if (err) {
              const queries = succeededTransactionSteps
                  .filter((step) => step.undoQuery);
              if (!queries.length) next(new Error('Failed to register user'));
              else {
                console.log({err});
                // delete all sqls
                const promises = [];
                queries.forEach((step) => {
                  console.log({step});
                  promises.push(
                      new Promise((resolve, rej) => db
                          .query(step.undoQuery, (queryErr, queryResult) => {
                            if (queryErr) {
                              console.log({queryErr});
                              rej(queryErr);
                            } else resolve();
                            // queryErr ? rej() : res()
                          })),
                  );
                });
                Promise.all(promises)
                    .then(() => next(
                        new Error('Failed to register user')),
                    )
                    .catch((e) => next(
                        new Error(`Failed to register user ${e.message}`)),
                    );
              }
            } else {
              const userInfo = queryResult.rows[0];
              userInfo.password = undefined;
              // user.email = AESController.encrypt(user.email)
              console.log(userInfo);
              res.status(200).json({status: 200, payload: {userInfo}});
            }
          },
      );
    }


    db.query(
        'SELECT * FROM users WHERE email = $1',
        [email],
        function(userInfoErr, userInfoResult) {
          if (userInfoErr) {
            next(err);
          } else {
            const userExists = userInfoResult.rows.length > 0 ? true : false;
            if (userExists) {
              const userInfo = userInfoResult.rows[0];
              res.status(200).json({status: 200, payload: {userInfo}});
            } else {
              UserCreationTransaction.forEach((func) => func());
            }
          }
        },
    );
  }

  /**
   * @method GET
   * @param {object} req
   * @param {object} res
   */
  static googleFail(req, res) {
    req = null;
  }

  /**
   * @method GET
   * @param {object} req
   * @param {object} res
   */
  static googleLogout(req, res) {
    req.session = null;
    req.logout();
    res.redirect('/');
  }
}

export default Auth2Controller;
