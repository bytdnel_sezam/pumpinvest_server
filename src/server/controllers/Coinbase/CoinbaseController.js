import {wrappedResponse} from '../../../helpers/index.js';
import CoinbasePayment from '../../../api/payment/coinbase/CoinbasePayment.js';
import {
  saveUserCryptoCharge,
  deleteCryptoCharge,
  getUserByCryptoCharge,
  addDepositTransaction,
  increaseUserBalance,
  undoQuery,
} from './CoinbaseControllerFunctions.js';

/**
 * @class CoinbaseController
 */
class CoinbaseController {
  /**
   * @method POST
   *
   * @param {object} req
   * @param {object} res
   */
  static async getPaymentForm(req, res) {
    try {
      const userInfo = req.JWT.payload;
      const {amount} = req.body;

      const coinbaseCharge = await CoinbasePayment.createCharge(amount);
      await saveUserCryptoCharge(userInfo.uid, coinbaseCharge.id);
      const paymentLink = coinbaseCharge.hosted_url;

      wrappedResponse(res, 200, {paymentLink});
    } catch (error) {
      wrappedResponse(res, 500, {error});
    }
  }

  /**
   * @method POST
   *
   * @param {object} req
   * @param {object} res
   */
  static async webhookSuccess(req, res) {
    const undoQueries = [];
    try {
      const webhook = req.body;
      const chargeId = webhook.event.data.id;
      const amount = webhook.event.data.pricing.local.amount;
      console.log({
        WebhookSuccess: webhook,
        eventData: webhook.event.data,
      });
      const uid = await getUserByCryptoCharge(chargeId);
      console.log({
        uid, chargeId, amount,
      });

      const undoTransaction = await addDepositTransaction(
          uid, chargeId, amount,
      );
      undoQueries.unshift(undoTransaction);

      await increaseUserBalance(uid, amount);
      await deleteCryptoCharge(uid, chargeId);

      wrappedResponse(res, 200, {message: 'OK'});
    } catch (error) {
      for (const block of undoQueries) {
        for (const query of block) {
          await undoQuery(query);
        }
      };
      wrappedResponse(res, 500, {error});
    }
  }

  /**
   * @method POST
   *
   * @param {object} req
   * @param {object} res
   */
  static async webhookFail(req, res) {
    try {
      const webhook = req.body;
      const chargeId = webhook.event.data.id;
      console.log({
        WebhookFail: webhook,
        eventData: webhook.event.data,
      });
      const uid = await getUserByCryptoCharge(chargeId);
      await deleteCryptoCharge(uid, chargeId);
      console.log({
        uid, chargeId,
      });
      wrappedResponse(res, 200, {message: 'OK'});
    } catch (error) {
      wrappedResponse(res, 500, {error});
    }
  }
}

export default CoinbaseController;
