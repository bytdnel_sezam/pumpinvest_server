import db from '../../../database/database.js';
import {types} from '../../../helpers/index.js';

/**
 * @param {number} uid
 * @param {string} chargeId
 */
export async function saveUserCryptoCharge(uid, chargeId) {
  try {
    await db.query(
        `INSERT INTO crypto_charges
          (uid, charge_id)
        VALUES ($1, $2)`,
        [uid, chargeId],
    );
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 *
 * @param {number} uid
 * @param {string} chargeId
 */
export async function deleteCryptoCharge(uid, chargeId) {
  try {
    await db.query(
        `DELETE FROM crypto_charges
        WHERE uid = $1 AND charge_id = $2`,
        [uid, chargeId],
    );
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 *
 * @param {string} chargeId
 */
export async function getUserByCryptoCharge(chargeId) {
  try {
    const queryResult = await db.query(
        `SELECT uid FROM crypto_charges
        WHERE charge_id = $1`,
        [chargeId],
    );
    const uid = queryResult.rows[0].uid;
    if (uid) return uid;
    else throw new Error('Uid is undefined!');
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 *
 * @param {number} uid
 * @param {string} chargeId
 * @param {number} amount
 * @return {Promise<array>} Undo query
 */
export async function addDepositTransaction(uid, chargeId, amount) {
  try {
    const billId = chargeId;
    // what if billId collision with other payment methods?
    await db.query(
        `INSERT INTO transactions
          (
            uid,
            transaction_amount,
            currency_type,
            transaction_type_id,
            transaction_status_id,
            bill_id
          )
        VALUES ($1, $2, $3, $4, $5, $6)`,
        [
          uid,
          amount,
          'USD',
          types.TRANSACTION_TYPE.ID_BY_VALUE.DEPOSIT_INCOME,
          types.TRANSACTION_STATUS.ID_BY_VALUE.SUCCESS,
          billId,
        ],
    );

    const undoTransaction = [
      `DELETE FROM transactions 
      WHERE uid = ${uid} 
      AND bill_id = ${billId} 
      AND transaction_amount = ${amount}`,
    ];

    return undoTransaction;
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 *
 * @param {number} uid
 * @param {number} amount
 */
export async function increaseUserBalance(uid, amount) {
  try {
    await db.query(
        `UPDATE wallets
        SET balance = ROUND((balance + $1)::numeric, 6)
        WHERE uid = $2`,
        [amount, uid],
    );
  } catch (queryErr) {
    throw new Error(queryErr);
  }
}

/**
 *
 * @param {string} query
 */
export async function undoQuery(query) {
  return await db.query(query);
}
