/* eslint-disable quotes */
import db from '../../../database/database.js';
import {qiwiPaymentApi} from '../../../api/payment/qiwi/qiwi.js';
import {wrappedResponse} from '../../../helpers/index.js';
import {types} from '../../../helpers/index.js';

const {
  ROLE_TYPE,
  TRANSACTION_TYPE,
  TRANSACTION_STATUS,
} = types;

/**
 * @class PaymentController
 */
class QiwiPaymentController {
  /**
   *
   * @param {object} req
   * @param {object} res
   */
  static getPaymentForm(req, res) {
    console.log({req: req.JWT.payload, body: req.body});
    const {uid} = req.JWT.payload;
    const body = req.body;

    if (!body.amount) {
      const error = new Error();
      // eslint-disable-next-line max-len
      error.message = 'There is no [amount] param in your request. Fuck you little bitch.';
      wrappedResponse(
          res,
          500,
          {error},
      );
    } else {
      const {amount} = body;

      db.query(
          `SELECT role_id FROM users WHERE uid = ${uid}`,
          (roleIdErr, roleIdResult) => {
            if (roleIdErr) {
              const error = new Error();
              error.message = `[get_role_error]: ${roleIdErr.message}`;
              wrappedResponse(
                  res,
                  500,
                  {error},
              );
            } else {
              if (roleIdResult.rows.length != 0) { // if user exists ??
                const roleId = roleIdResult.rows[0].role_id;
                console.log({roleId});

                if (
                  roleId == ROLE_TYPE.ID_BY_VALUE.CONFIRMED ||
                  roleId == ROLE_TYPE.ID_BY_VALUE.ADMIN
                ) { // if role is correct
                  const billId = qiwiPaymentApi.generateId();

                  try {
                    const paymentLink = qiwiPaymentApi.createPaymentForm({
                      billId,
                      publicKey: qiwiPaymentApi.getPublicKey(),
                      amount,
                      account: uid,
                      successUrl: 'https://google.com/',
                      paySource: 'card',
                    });

                    wrappedResponse(res, 200, {paymentLink});
                  } catch (paymentLinkErr) {
                    wrappedResponse(res, 500, {error: paymentLinkErr});
                  }
                } else {
                  const error = new Error();
                  // eslint-disable-next-line max-len
                  error.message = '[user_permission_error]: User is not confirmed';
                  wrappedResponse(req, 403, {error});
                }
              } else {
                const error = new Error();
                // eslint-disable-next-line max-len
                error.message = '[user_error]: There is not user with same user_id and wallet_id';
                wrappedResponse(req, 400, {error});
              }
            }
          });
    }
  }

  /**
   *
   * @param {object} req
   * @param {object} res
   */
  static async paymentSuccess(req, res) {
    console.log('Payment success callback!');
    const bill = req.body.bill;
    console.log({bill});
    const uid = bill.customer.account;
    const queryResult = await db.query(
        "SELECT val FROM currency_ratios WHERE ratio = 'usd_rub'",
    );
    const USD_RUB = queryResult.rows[0].val;
    const usdAmount = Number((Number(bill.amount.value) / USD_RUB).toFixed(6));

    console.log({usdAmount});

    db.query(
        `INSERT INTO transactions 
          (uid,
          transaction_amount,
          currency_type,
          transaction_type_id,
          transaction_status_id,
          date_created,
          bill_id)
        VALUES ($1, $2, $3, $4, $5, $6, $7)`,
        [
          uid,
          usdAmount,
          'USD',
          TRANSACTION_TYPE.ID_BY_VALUE.DEPOSIT_INCOME,
          TRANSACTION_STATUS.ID_BY_VALUE.SUCCESS,
          (new Date()).toISOString(),
          bill.billId,
        ],
        (transactionIdErr) => {
          if (transactionIdErr) {
            console.log({transactionIdErr});
          } else {
            db.query( // balance + $1 is DEPRECATED
                `UPDATE wallets 
                SET balance = ROUND((balance + $1)::numeric, 6)
                WHERE uid = $2 AND currency_type = $3`,
                [usdAmount, uid, 'USD'],
            )
                .then((result) => wrappedResponse(res, 200, {message: 'OK'}))
                .catch((error) => wrappedResponse(res, 500, {error}));
          }
        },
    );
  }
}

export default QiwiPaymentController;
