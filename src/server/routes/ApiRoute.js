import {Router} from 'express';
import ApiController from '../controllers/Api/ApiController.js';
import {verifyJwtBearer, allowOnlyAdmin} from '../../helpers/index.js';
// eslint-disable-next-line new-cap
const router = Router();

router.get(
    '/getUserTransactions',
    verifyJwtBearer,
    ApiController.getUserTransactions,
);
router.post(
    '/withdrawalRequest',
    verifyJwtBearer,
    ApiController.withdrawalRequest,
);

router.get(
    '/getAllRequestedTransactions',
    [verifyJwtBearer, allowOnlyAdmin],
    ApiController.getAllRequestedTransactions,
);
router.post(
    '/withdrawMoney',
    [verifyJwtBearer, allowOnlyAdmin],
    ApiController.withdrawMoney,
);

export const ApiRoute = router;
