import {Router} from 'express';
import Auth0Controller from '../controllers/Auth0/Auth0Controller.js';
import {verifyJwtBearer} from '../../helpers/index.js';
// eslint-disable-next-line new-cap
const router = Router();


router.post('/register', Auth0Controller.register);
router.post('/login', Auth0Controller.login);
router.get('/fetchAuth', verifyJwtBearer, Auth0Controller.fetchAuth);
router.get('/confirmUser', Auth0Controller.confirmUser);

export const Auth0Route = router;
