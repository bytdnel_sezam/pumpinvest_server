/* eslint-disable max-len */
import {Router} from 'express';
import Auth2Controller from '../controllers/Auth2/Auth2Controller.js';
import passport from 'passport';
import GoogleStrategy from 'passport-google-oauth20';
// import cookieParser from 'cookie-parser';
import Cookies from 'cookies';

// eslint-disable-next-line new-cap
const router = Router();

passport.serializeUser(function(user, done) {
  console.log('SERIALIZE');
  // console.log(arguments)
  /*
    From the user take just the id (to minimize the cookie size) and just pass the id of the user
    to the done callback
    PS: You don't have to do it like this its just usually done like this
    */
  console.log({user});
  done(null, user);
});
passport.deserializeUser(function(user, done) {
  console.log('DESERIALIZE');
  /*
    Instead of user this function usually receives the id
    then you use the id to select the user from the db and pass the user obj to the done callback
    PS: You can later access this data in any routes in: req.user
    */
  done(null, user);
});
passport.use(new GoogleStrategy.Strategy(
    {
      clientID: '264403957513-n8654o16hh11l0v7m8qhh2d8l2aroia2.apps.googleusercontent.com',
      clientSecret: 'U8BRSB3KvH3qBLEJFvkdK9Xp',
      callbackURL: 'https://5-63-155-138.ovz.vps.regruhosting.ru/auth2/google/callback',
    },
    function(accessToken, refreshToken, profile, done) {
      // console.log('GOOGLE STRATEGY CALLBACK', {request, accessToken, refreshToken, profile})
      // console.log({profile})
      console.log({accessToken, refreshToken, profile});

      return done(null, profile, 'some info');
      // return done(null, profile);
    },
));

const isLoggedIn = (req, res, next) => {
  if (req.user) {
    next();
  } else {
    const keys = ['key1', 'key2'];
    const cookies = new Cookies(req, res, {keys});
    const refid = req.query.refid;
    if (refid) cookies.set('refid', `${refid}`, {maxAge: 1000*60*30});
    res.redirect('/auth2/google');
  }
};

router.get('/failed', (req, res) => {
  return Auth2Controller.googleFail;
});

router.get(
    '/fetchAuth',
    isLoggedIn,
    (req, res) => res.send(`Welcome mr ${req.user.displayName}!`),
);

router.get(
    '/google',
    passport.authenticate(
        'google',
        {scope: ['profile', 'email']},
        (err, user, info) => {
          console.log({err, user, info});
        },
    ),
);

// router.get('/google', passport.authenticate('google', { scope: ['profile', 'email']}))

router.get(
    '/google/callback',
    passport.authenticate(
        'google',
        {failureRedirect: '/auth2/failed'},
    ),
    Auth2Controller.googleCallback,
);

router.get('/logout', Auth2Controller.googleLogout);

export const Auth2Route = router;
