import {Router} from 'express';
import CoinbaseController from '../controllers/Coinbase/CoinbaseController.js';
import {verifyJwtBearer} from '../../helpers/index.js';

const router = new Router();

router.post('/webhook/success', CoinbaseController.webhookSuccess);
router.post('/webhook/fail', CoinbaseController.webhookFail);
router.post(
    '/getPaymentUrl',
    verifyJwtBearer,
    CoinbaseController.getPaymentForm,
);

export const CoinbaseRoute = router;
