import {Router} from 'express';
// eslint-disable-next-line max-len
import QiwiPaymentController from '../controllers/QiwiPayment/QiwiPaymentController.js';
import {verifyJwtBearer} from '../../helpers/index.js';

const router = new Router();

router.post(
    '/getPaymentForm',
    verifyJwtBearer,
    QiwiPaymentController.getPaymentForm,
);

router.post(
    '/paymentSuccess',
    QiwiPaymentController.paymentSuccess,
);

export const QiwiRoute = router;
