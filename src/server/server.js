// eslint-disable-next-line no-unused-vars
import cors from 'cors';
import passport from 'passport';
import cookieSession from 'cookie-session';
import express from 'express';
import compression from 'compression';
import {Auth0Route} from './routes/Auth0Route.js';
import {Auth2Route} from './routes/Auth2Route.js';
import {QiwiRoute} from './routes/QiwiRoute.js';
import {ApiRoute} from './routes/ApiRoute.js';
import {CoinbaseRoute} from './routes/CoinbaseRoute.js';

/**
 * This is init function for the entire server
 * @param {integer} port
 */
export default function initServer(port) {
  const app = express();
  // app.use(cors({origin: '*'}))
  app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT');
    // eslint-disable-next-line max-len
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
  });
  app.use(express.json());
  app.use(compression());
  app.use(cookieSession({
    name: 'oauth2/google',
    keys: ['key1', 'key2', 'key3'],
  }));
  app.use(passport.initialize());
  app.use(passport.session());

  app.use('/auth0', Auth0Route);
  app.use('/auth2', Auth2Route);
  app.use('/qiwi', QiwiRoute);
  app.use('/api', ApiRoute);
  app.use('/coinbase', CoinbaseRoute);

  app.use((err, req, res, next) => {
    if (err) {
      const serverStatus = err.status || 500;
      res.status(serverStatus).json({
        success: false,
        payload: {
          error: {
            status: serverStatus,
            message: err.message,
          },
        },
      });
    }
  });

  app.all('/coinbase/webhook', (req, res) => {
    console.log('RECEIVED WEBHOOK!', req.body);
    res.status(200).send('OK');
  });

  app.get('/', (req, res) => {
    res.send('is it through https?');
  });

  app.listen(port, () => {
    console.log(`Listening port ${port}`);
  });
}
