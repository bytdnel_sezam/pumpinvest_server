import {workerData} from 'worker_threads';
import Mailer from '../api/emailController.js';
import AESController from '../api/AESController.js';

const emails = workerData.emails;
console.log(emails);

const promises = [];
emails.forEach((email) => {
  promises.push(new Promise((res, rej) => {
    const emailEncrypted = AESController.encrypt(email);
    console.log({confirmationLink: `https://5-63-155-138.ovz.vps.regruhosting.ru/auth0/confirmUser?token=${emailEncrypted}`});
    console.log({email, emailEncrypted});

    Mailer.sendConfirmation(

        {
          to: emails.reduce((str, part) => {
            if (str != '') return str += `, ${part}`;
            else return str = part;
          }, ''),
          subject: 'Подтверждение email | Pumpinvest',
          text: `https://5-63-155-138.ovz.vps.regruhosting.ru/auth0/confirmUser?token=${emailEncrypted}`,
        },

        (err, info) => {
          if (err) rej(err);
          else res(info);
        },

    );
  }));
});

Promise.all(promises)
    .then(() => console.log('all emails sent'))
    .catch((e) => console.log({e}));
